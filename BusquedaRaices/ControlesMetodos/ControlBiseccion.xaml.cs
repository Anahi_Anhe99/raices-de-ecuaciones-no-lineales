﻿using BusquedaRaices.Modelos;
using ManejoFunciones;
using ManejoFunciones.Entidades;
using ManejoFunciones.Metodos.Raices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BusquedaRaices.ControlesMetodos
{
    /// <summary>
    /// Lógica de interacción para ControlBiseccion.xaml
    /// </summary>
    public partial class ControlBiseccion : UserControl, ManejarDatos
    {
        ControlGraficar controlGraficar;
        EvaluarFunciones evaluarFunc;
        MetodoBiseccion buscarRaices;
        List<TablaBiseccion> datos;
        ConvertirCSV<TablaBiseccion> csv;
        string raiz;
        public ControlBiseccion()
        {
            InitializeComponent();
            controlGraficar = new ControlGraficar();
            datos = new List<TablaBiseccion>();
            evaluarFunc = new EvaluarFunciones();
            buscarRaices = new MetodoBiseccion();
            csv = new ConvertirCSV<TablaBiseccion>(@"C:\csv\metodoBiseccion\raiz.csv");
        }

        public bool Exportar()
        {
            //Exporta los datos a un csv
            if (csv.ExportarCSV("i,xi,xd,x,|f(x)|,|x-xd|", datos))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void HabilitarCampos()
        {
            wppDatos.IsEnabled = true;
        }

        public void LimpiarCampos()
        {

        }

        public string Procesar(string funcion, out string iteraciones)
        {
            try
            {
                double eps = evaluarFunc.EvaluarConstantes(txb_e.Text);
                //Valida que el creterio de convergencia y exactitud sean válidos
                if (int.Parse(txb_maxit.Text) > 1000000000 || eps.ToString() == "NaN")
                {
                    MessageBox.Show("Lo siento pero el numero de iteraciones que intentas ingresar, puede provocar un desbordamiento de memoria", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                raiz = buscarRaices.MetodoBiseccionn(funcion, double.Parse(txb_xi.Text), double.Parse(txb_xd.Text), eps, int.Parse(txb_maxit.Text), out int total_It, out datos);
                iteraciones = total_It.ToString();
                return raiz;
            }
            catch (Exception)
            {
                MessageBox.Show("Asegurate de que:\n Los valores esten en el formato correcto\n- No sean demasiado grandes\n- No sean demasiado pequeños\n- Todos los campos esten llenos", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                iteraciones = "0";
                return "";
            }
        }
    }
}
