﻿using BusquedaRaices.Modelos;
using ManejoFunciones;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BusquedaRaices.ControlesMetodos
{
    /// <summary>
    /// Lógica de interacción para ControlGraficar.xaml
    /// </summary>
    public partial class ControlGraficar : UserControl
    {

        EvaluarFunciones evaluar;
        ManejarDatos manejar;
        string mensaje;
        string iteraciones;
        public ControlGraficar()
        {

        }
        public ControlGraficar(ManejarDatos _manejar)
        {
            InitializeComponent();
            manejar = _manejar;
            evaluar = new EvaluarFunciones();
        }
        private void BtnProcesar_Click(object sender, RoutedEventArgs e)
        {
            txb_raixaprox.Text = manejar.Procesar(txb_fx.Text, out iteraciones);
            txb_totaliteraciones.Text = iteraciones;
            btnExportar.IsEnabled = true;
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            if (manejar.Exportar()
)
            {
                MessageBox.Show("Los datos se han exportado con éxito", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            else
            {
                MessageBox.Show("Ya haz mi programa Mario :'c", "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void BtnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            txb_fx.Text = "";
            txb_a.Text = "";
            txb_b.Text = "";
            txb_raixaprox.Text = "";
            txb_totaliteraciones.Text = "";
            manejar.LimpiarCampos();
        }

        private void BtnIntegrantes_Click(object sender, RoutedEventArgs e)
        {
            //Muestra los integrantes del equipo
            MessageBox.Show("-Angeles Hernández Anahi\n-Badillo López Cyndi\n-Martínez Quintanar Alain Jordan\n-Ramirez Mejía Ernesto", "Integrantes", MessageBoxButton.OK, MessageBoxImage.Information);

        }

        public void Graficar()
        {
            double int_a = evaluar.EvaluarConstantes(txb_a.Text);
            double int_b = evaluar.EvaluarConstantes(txb_b.Text);
            //Valida que los intervalos sean correctos y que cumpla con que a<b
            if (int_a.ToString() == "NaN" || int_b.ToString() == "NaN" || int_b <= int_a)
            {
                MessageBox.Show("- El intervalo debe cumplir a < b", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            //Crea una lista de puentos que se ocupan ´para pinar la grafica
            List<Punto> resultado = evaluar.TabularFuncion(txb_fx.Text, out mensaje, int_a, int_b, 0.1f);
            if (mensaje == "OK")
            {
                PlotModel model = new PlotModel();
                LineSeries line = new LineSeries();
                LineSeries ejeX = new LineSeries();
                LineSeries ejeY = new LineSeries();
                ejeX.Points.Add(new DataPoint(0, 0));
                ejeY.Points.Add(new DataPoint(0, 0));
                //Agrega cada punto al Plotmodel
                foreach (var item in resultado)
                {
                    if (item.X.ToString() == "NaN" || item.Y.ToString() == "NaN")
                    {
                        MessageBox.Show("Es posible que:\n-La funcion esté escrita incorrectamente\n-La función en los intervalos especificados sea discontinua\n-Verifica tus entradas, e intenta otra vez", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    line.Points.Add(new DataPoint(item.X, item.Y));
                    ejeX.Points.Add(new DataPoint(item.X, 0f));
                    ejeY.Points.Add(new DataPoint(0f, item.Y));
                }
                ejeX.Color = OxyColors.Black;
                ejeY.Color = OxyColors.Black;
                model.Series.Add(ejeX);
                model.Series.Add(ejeY);
                line.Title = txb_fx.Text;
                line.Color = OxyColors.BlueViolet;
                model.Series.Add(line);
                //Pinta la grafica
                plotGrafica.Model = model;
                manejar.HabilitarCampos();
                btnProcesar.IsEnabled = true;
                btnLimpiar.IsEnabled = true;

            }
            else
            {
                MessageBox.Show(mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void BtnGraficar_Click(object sender, RoutedEventArgs e)
        {
            Graficar();
            wppDatos.IsEnabled = true;
        }
        
    }
}
