﻿using BusquedaRaices.Modelos;
using ManejoFunciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BusquedaRaices.ControlesMetodos
{
    /// <summary>
    /// Lógica de interacción para ControlNewtonRapshon.xaml
    /// </summary>
    public partial class ControlNewtonRapshon : UserControl, ManejarDatos
    {
        ControlGraficar controlGraficar;
        EvaluarFunciones evaluarFunc;
        MetodoNewton buscarRaices;
        List<TablaNewton> datos;
        ConvertirCSV<TablaNewton> csv;
        string raiz;
        
        public ControlNewtonRapshon()
        {
            InitializeComponent();
            controlGraficar = new ControlGraficar();
            datos = new List<TablaNewton>();
            evaluarFunc = new EvaluarFunciones();
            buscarRaices = new MetodoNewton();
            csv = new ConvertirCSV<TablaNewton>(@"C:\csv\metodoNewton\1.csv");
        }

        public bool Exportar()
        {
            //Exporta los datos a un csv
            if (csv.ExportarCSV("i,xi,g(xi),|x1-x0|,f(xi)", datos))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void HabilitarCampos()
        {
            wrpDatos.IsEnabled = true;
        }

        public void LimpiarCampos()
        {
            txb_e.Text = "";
            txb_e1.Text = "";
            txb_maxit.Text = "";
            txb_x0.Text = "";
        }

        public string Procesar(string funcion, out string iteraciones)
        {
            try
            {
                double eps = evaluarFunc.EvaluarConstantes(txb_e.Text);
                double eps1 = evaluarFunc.EvaluarConstantes(txb_e.Text);
                //Valida que el creterio de convergencia y exactitud sean válidos
                if (int.Parse(txb_maxit.Text) > 1000000000 || eps.ToString() == "NaN" || eps1.ToString() == "NaN")
                {
                    MessageBox.Show("Lo siento pero el numero de iteraciones que intentas ingresar, puede provocar un desbordamiento de memoria", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                raiz = buscarRaices.MetodoNewtonRapshon(funcion, double.Parse(txb_x0.Text), int.Parse(txb_maxit.Text), eps, eps1, out int total_It, out datos);
                iteraciones = total_It.ToString();
                return raiz;
            }
            catch (Exception)
            {
                MessageBox.Show("Asegurate de que:\n Los valores esten en el formato correcto\n- No sean demasiado grandes\n- No sean demasiado pequeños\n- Todos los campos esten llenos", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                iteraciones = "0";
                return "";
            }
        }
    }
}
