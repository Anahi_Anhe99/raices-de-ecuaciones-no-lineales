﻿using BusquedaRaices.Modelos;
using ManejoFunciones;
using ManejoFunciones.Entidades;
using ManejoFunciones.Metodos.Raices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BusquedaRaices.ControlesMetodos
{
    /// <summary>
    /// Lógica de interacción para ControlPosicionFalsa.xaml
    /// </summary>
    public partial class ControlPosicionFalsa : UserControl, ManejarDatos
    {
        EvaluarFunciones evalua;
        List<TablaPosicionFalsa> datos;
        MetodoPosicionFalsa buscarRaices;
        ConvertirCSV<TablaPosicionFalsa> csv;
        string raiz;
        public ControlPosicionFalsa()
        {
            InitializeComponent();
            evalua = new EvaluarFunciones();
            datos = new List<TablaPosicionFalsa>();
            buscarRaices = new MetodoPosicionFalsa();
            csv = new ConvertirCSV<TablaPosicionFalsa>(@"C:\csv\metodoPosicionFalsa\1.csv");
        }

        public bool Exportar()
        {
            //Exporta los datos a un csv por medio de la clase ConvertirCSV
            if (csv.ExportarCSV("i,xi,xd,x,|xd-xi|,|f(x)|", datos))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void HabilitarCampos()
        {
            wppDatos.IsEnabled = true;
        }

        public void LimpiarCampos()
        {
            throw new NotImplementedException();
        }

        public string Procesar(string funcion, out string iteraciones)
        {
            try
            {
                double eps = evalua.EvaluarConstantes(txb_e.Text);
                double eps1 = evalua.EvaluarConstantes(txb_e1.Text);
                double xi = evalua.EvaluarConstantes(txb_xi.Text);
                double xd = evalua.EvaluarConstantes(txb_xd.Text);

                //Valida que el creterio de convergencia y exactitud sean válidos
                if (int.Parse(txb_maxit.Text) > 1000000000 || eps.ToString() == "NaN" || eps1.ToString() == "NaN" || xd.ToString() == "NaN" || xi.ToString() == "NaN")
                {
                    MessageBox.Show("Lo siento pero el numero de iteraciones que intentas ingresar, puede provocar un desbordamiento de memoria", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    iteraciones = "";
                    return "";
                }
                raiz = buscarRaices.MetodoPosiciónFalsaa(funcion, xi, xd, eps, eps1, int.Parse(txb_maxit.Text), out int total_It, out datos);
                iteraciones = total_It.ToString();
                return raiz;
            }

            catch
            {
                MessageBox.Show("Asegurate de que:\n Los valores esten en el formato correcto\n- No sean demasiado grandes\n- No sean demasiado pequeños\n- Todos los campos esten llenos", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                iteraciones = "0";
                return "";
            }
        }
    }
}


