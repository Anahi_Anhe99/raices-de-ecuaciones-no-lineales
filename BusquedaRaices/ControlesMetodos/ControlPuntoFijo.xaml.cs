﻿using BusquedaRaices.Modelos;
using ManejoFunciones;
using ManejoFunciones.Entidades;
using ManejoFunciones.Metodos.Raices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BusquedaRaices.ControlesMetodos
{
    /// <summary>
    /// Lógica de interacción para ControlPuntoFijo.xaml
    /// </summary>
    public partial class ControlPuntoFijo : UserControl, ManejarDatos
    {
        MetodoPuntoFijo buscarRaices;
        EvaluarFunciones evalua;
        List<TablaPuntoFijo> datos;
        ConvertirCSV<TablaPuntoFijo> csv;
        string raiz;
        public ControlPuntoFijo()
        {
            InitializeComponent();
            buscarRaices = new MetodoPuntoFijo();
            datos = new List<TablaPuntoFijo>();
            evalua = new EvaluarFunciones();
            csv = new ConvertirCSV<TablaPuntoFijo>(@"C:\csv\metodoPuntoFijo\1.csv");
        }

        public bool Exportar()
        {
            //Exporta los datos a un csv
            if (csv.ExportarCSV("i,xi,g(xi),f(xi),|x-x0|", datos))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void HabilitarCampos()
        {
            wrpDatos.IsEnabled = true;
        }

        public void LimpiarCampos()
        {
            throw new NotImplementedException();
        }

        public string Procesar(string funcion, out string iteraciones)
        {
            try
            {
                double eps = evalua.EvaluarConstantes(txb_e.Text);
                double x0 = evalua.EvaluarConstantes(txb_x0.Text);
                double gx = Math.Abs(evalua.DerivarFuncion(txb_gx.Text, x0));
                
                if (gx>=1)
                {
                    MessageBox.Show("El valor propuesto para x no es el más óptimo.\nM > 1", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    iteraciones = "";
                    return "";
                }
                //Valida que el creterio de convergencia y exactitud sean válidos
                if (int.Parse(txb_maxit.Text) > 1000000000 || eps.ToString() == "NaN" || x0.ToString()=="NaN")
                {
                    MessageBox.Show("Lo siento pero el numero de iteraciones que intentas ingresar, puede provocar un desbordamiento de memoria", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    iteraciones = "";
                    return "";
                }
                raiz = buscarRaices.MetodoPuntoFijoo(funcion, txb_gx.Text, x0, eps, int.Parse(txb_maxit.Text), out int total_It, out datos);
                iteraciones = total_It.ToString();
                return raiz;
            }
            catch (Exception)
            {
                MessageBox.Show("Asegurate de que:\n Los valores esten en el formato correcto\n- No sean demasiado grandes\n- No sean demasiado pequeños\n- Todos los campos esten llenos", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                iteraciones = "0";
                return "";
            }
        }
    }
}
