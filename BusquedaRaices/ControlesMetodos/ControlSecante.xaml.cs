﻿using BusquedaRaices.Modelos;
using ManejoFunciones;
using ManejoFunciones.Entidades;
using ManejoFunciones.Metodos.Raices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BusquedaRaices.ControlesMetodos
{
    /// <summary>
    /// Lógica de interacción para ControlSecante.xaml
    /// </summary>
    public partial class ControlSecante : UserControl, ManejarDatos
    {
        ControlGraficar controlGraficar;
        EvaluarFunciones evaluarFunciones;
        MetodoSecante buscarRaices;
        List<TablaSecante> datos;
        ConvertirCSV<TablaSecante> csv;
        string raiz;
        public ControlSecante()
        {
            InitializeComponent();
            controlGraficar = new ControlGraficar();
            datos = new List<TablaSecante>();
            evaluarFunciones = new EvaluarFunciones();
            buscarRaices = new MetodoSecante();
            csv = new ConvertirCSV<TablaSecante>(@"C:\csv\metodoSecante\raiz.csv");
        }

        public bool Exportar()
        {
            //Exporta los datos a un csv
            if (csv.ExportarCSV("i,x0,x1,x,f(x),|x-x1|", datos))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void HabilitarCampos()
        {
            wppDatos.IsEnabled = true;
        }

        public void LimpiarCampos()
        {
            txbe.Text = "";
            txbe1.Text = "";
            txbmaxit.Text = "";
            txbx0.Text = "";
            txbx1.Text = "";
        }

        public string Procesar(string funcion, out string iteraciones)
        {
            try
            {
                double eps = evaluarFunciones.EvaluarConstantes(txbe.Text);
                double eps1 = evaluarFunciones.EvaluarConstantes(txbe1.Text);
                if (eps.ToString() == "NaN" || eps1.ToString() == "NaN")
                {
                    MessageBox.Show("Revisa tus entradas", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    iteraciones = "";
                    return "";
                }
                if (!int.TryParse(txbmaxit.Text, out int maxit))
                {
                    MessageBox.Show("Lo siento pero el número de iteraciones que deseas ingresar supera los limites");
                    iteraciones = "";
                    return "";
                }
                raiz = buscarRaices.MetodoSecantee(funcion, double.Parse(txbx0.Text), double.Parse(txbx1.Text), eps, eps1, maxit, out int total_It, out datos);
                iteraciones = total_It.ToString();
                return raiz;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                iteraciones = "0";
                return "";
            }
        }
    }
}
