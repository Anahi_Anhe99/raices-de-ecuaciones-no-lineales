﻿using BusquedaRaices.Modelos;
using ManejoFunciones;
using ManejoFunciones.Entidades;
using ManejoFunciones.Metodos.Raices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BusquedaRaices.ControlesMetodos
{
    /// <summary>
    /// Lógica de interacción para ControlSteffensen.xaml
    /// </summary>
    public partial class ControlSteffensen : UserControl, ManejarDatos
    {
        EvaluarFunciones evaluarFunc;
        MetodoSteffensen buscarRaices;
        List<TablaSteffensen> datos;
        ConvertirCSV<TablaSteffensen> csv;
        string raiz;
        public ControlSteffensen()
        {
            InitializeComponent();
            evaluarFunc = new EvaluarFunciones();
            buscarRaices = new MetodoSteffensen();
            datos = new List<TablaSteffensen>();
            csv = new ConvertirCSV<TablaSteffensen>(@"C:\csv\metodoSteffensen\raiz.csv");
        }

        public bool Exportar()
        {
            //Exporta los datos a un csv

            if (csv.ExportarCSV("i,x0,x1,x2,x,abs,absf", datos))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void HabilitarCampos()
        {
            wppDatos.IsEnabled = true;
        }

        public void LimpiarCampos()
        {
            throw new NotImplementedException();
        }

        public string Procesar(string funcion, out string total_it)
        {
            try
            {
                double eps = evaluarFunc.EvaluarConstantes(txb_e.Text);
                //Valida que el creterio de convergencia y exactitud sean válidos
                if (int.Parse(txb_maxit.Text) > 1000000000 || eps.ToString() == "NaN")
                {
                    MessageBox.Show("Lo siento pero el numero de iteraciones que intentas ingresar, puede provocar un desbordamiento de memoria", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                raiz = buscarRaices.MetodoSteffenseen(funcion, txb_G.Text, double.Parse(txb_x0.Text), eps, int.Parse(txb_maxit.Text), out int total_It, out datos);
                total_it = total_It.ToString();
                return raiz;
            }
            catch (Exception)
            {
                total_it = "";
                MessageBox.Show("Hacen falta campos por rellenar", "Metodo de la secante", MessageBoxButton.OK, MessageBoxImage.Error);
                return "";
            }
        }
    }
}
