﻿using BusquedaRaices.ControlesMetodos;
using BusquedaRaices.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BusquedaRaices
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            LlenarCombo();

        }

        private void LlenarCombo()
        {
            cmbMetodos.Items.Add("Metodo Punto fijo");
            cmbMetodos.Items.Add("Metodo Newton Rapshon");
            cmbMetodos.Items.Add("Metodo Secante");
            cmbMetodos.Items.Add("Metodo Posición falsa");
            cmbMetodos.Items.Add("Metodo Bisección");
            cmbMetodos.Items.Add("Metodo Steffensen");
            cmbMetodos.Items.Add("Metodo Illinois");
        }

        private void CmbMetodos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string seleccion = cmbMetodos.SelectedItem.ToString();
            switch (seleccion)
            {
                case "Metodo Punto fijo":
                    lblTitulo.Content = seleccion;
                    MostrarControl(new ControlPuntoFijo());
                    break;
                case "Metodo Newton Rapshon":
                    lblTitulo.Content = seleccion;
                    MostrarControl(new ControlNewtonRapshon());

                    break;
                case "Metodo Secante":
                    lblTitulo.Content = seleccion;
                    MostrarControl(new ControlSecante());

                    break;
                case "Metodo Posición falsa":
                    lblTitulo.Content = seleccion;
                    MostrarControl(new ControlPosicionFalsa());

                    break;
                case "Metodo Bisección":
                    lblTitulo.Content = seleccion;
                    MostrarControl(new ControlBiseccion());
                    break;
                case "Metodo Steffensen":
                    lblTitulo.Content = seleccion;
                    MostrarControl(new ControlSteffensen());
                    break;
                case "Metodo Illinois":
                    lblTitulo.Content = seleccion;
                    MostrarControl(new ControlIllinois());
                    break;
                default:
                    break;
            }
        }
        public void MostrarControl(Control control)
        {
            skpContenedor.Children.Clear();
            ControlGraficar graficar = new ControlGraficar(control as ManejarDatos);
            graficar.wppDatos.Children.Clear();
            graficar.wppDatos.Children.Add(control);
            skpContenedor.Children.Add(graficar);
        }
    }
}
