﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusquedaRaices.Modelos
{
    public interface ManejarDatos
    {
        string Procesar(string funcion, out string iteraciones);
        void HabilitarCampos();
        void LimpiarCampos();
        bool Exportar();
    }
}
