﻿using ManejoFunciones;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Windows;

namespace General
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        EvaluarFunciones evaluar;
        MetodoNewton buscar;
        ConvertirCSV csv = new ConvertirCSV();
        List<TablaNewton> datos;
        float max;
        float min;
        string mensaje = "";
        public MainWindow()
        {
            InitializeComponent();
            evaluar = new EvaluarFunciones();
            buscar = new MetodoNewton();
            btnIntegrantes.IsEnabled = true;
            btnSalir.IsEnabled = true;
            datos = new List<TablaNewton>();
        }

        private void BtnGraficar_Click(object sender, RoutedEventArgs e)
        {
            //Grafica la función
            Graficar();
            datos.Clear();
        }
        private void Graficar()
        {
            double min = evaluar.EvaluarConstantes(txb_a.Text);
            double max = evaluar.EvaluarConstantes(txb_b.Text);
            //Valida que los intervalos sean correctos y que cumpla con que a<b
            if (min.ToString()=="NaN"|| max.ToString() == "NaN" || max <= min)
            {
                MessageBox.Show("- El intervalo debe cumplir a < b", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            //Crea una lista de puentos que se ocupan ´para pinar la grafica
            List<Punto> resultado = evaluar.TabularFuncion(txb_fx.Text, out mensaje, min, max, 0.1f);
            if (mensaje == "OK")
            {
                PlotModel model = new PlotModel();
                LineSeries line = new LineSeries();
                LineSeries ejeX = new LineSeries();
                LineSeries ejeY = new LineSeries();
                ejeX.Points.Add(new DataPoint(0,0));
                ejeY.Points.Add(new DataPoint(0,0));
                //Agrega cada punto al Plotmodel
                foreach (var item in resultado)
                {
                    if (item.X.ToString() == "NaN" || item.Y.ToString() == "NaN")
                    {
                        MessageBox.Show("Es posible que:\n-La funcion esté escrita incorrectamente\n-LA función en los intervalos especificados sea discontinua\n-Verifica tus entradas, e intenta otra vez", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    line.Points.Add(new DataPoint(item.X, item.Y));
                    ejeX.Points.Add(new DataPoint(item.X, 0f));
                    ejeY.Points.Add(new DataPoint(0f, item.Y));
                }
                ejeX.Color = OxyColors.Black;
                ejeY.Color = OxyColors.Black;
                model.Series.Add(ejeX);
                model.Series.Add(ejeY);
                line.Title = txb_fx.Text;
                line.Color = OxyColors.BlueViolet;
                model.Series.Add(line);
                //Pinta la grafica
                plotGrafica.Model = model;

            }
            else
            {
                MessageBox.Show(mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            btnProcesar.IsEnabled = true;
            btnLimpiar.IsEnabled = true;
            wrpDatos.IsEnabled = true;
            txb_fx.IsEnabled = false;

        }

        private void BtnProcesar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double eps= evaluar.EvaluarConstantes(txb_e.Text);
                double eps1= evaluar.EvaluarConstantes(txb_e.Text);
                //Valida que el creterio de convergencia y exactitud sean válidos
                if (int.Parse(txb_maxit.Text)>1000000||eps.ToString()=="NaN"|| eps1.ToString() == "NaN")
                {
                MessageBox.Show("Lo siento pero el numero de iteraciones que intentas ingresar, puede provocar un desbordamiento de memoria", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                txb_raixaprox.Text = buscar.MetodoNewtonRapshon(txb_fx.Text, double.Parse(txb_x0.Text), int.Parse(txb_maxit.Text),eps,eps1, out int total_It, out datos);
                txb_totaliteraciones.Text = total_It.ToString();
                btnExportar.IsEnabled = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Asegurate de que:\n Los valores esten en el formato correcto\n- No sean demasiado grandes\n- No sean demasiado pequeños\n- Todos los campos esten llenos", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Exporta los datos a un csv
                csv.ExportarCSV(datos, $"{DateTime.Now.ToString("dd-MM-yy hh_mm_ss")}Raices.csv", @"C:\csv\");
                MessageBox.Show("Los datos se han exportado con éxito", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (Exception ex)
            {
                //Notifica poque razón no se han podido exportar los datos y/o crar el csv
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            txb_fx.Text = "";
            txb_a.Text = "";
            txb_b.Text = "";
            txb_x0.Text = "";
            txb_e.Text = "";
            txb_e1.Text = "";
            txb_maxit.Text = "";
            txb_raixaprox.Text = "";
            txb_totaliteraciones.Text = "";
            plotGrafica.Model = null;
            wrpDatos.IsEnabled = false;
            btnProcesar.IsEnabled = false;
            btnExportar.IsEnabled = false;
            wrpGraficar.IsEnabled = true;
            txb_fx.IsEnabled = true;
            plotGrafica.Model = null;
        }
        

        private void BtnIntegrantes_Click(object sender, RoutedEventArgs e)
        {
            //Muestra los integrantes del equipo
            MessageBox.Show("-Angeles Hernández Anahi\n-Badillo López Cyndi\n-Martínez Quintanar Alain Jordan\n-Ramirez Mejía Ernesto", "Integrantes", MessageBoxButton.OK, MessageBoxImage.Information);

        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            //Cierra el programa
            this.Close();
        }
    }
}
