﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManejoFunciones
{
    public class TablaNewton
    {
        public string i { get; set; }
        public string xi { get; set; }
        public string gxi { get; set; }
        public string abs { get; set; }
        public string fxi { get; set; }
        public override string ToString()
        {
            return $"{i},{xi},{gxi},{abs},{fxi}";
        }

    }
}
