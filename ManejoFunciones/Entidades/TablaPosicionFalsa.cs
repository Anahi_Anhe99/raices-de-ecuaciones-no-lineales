﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManejoFunciones.Entidades
{
    public class TablaPosicionFalsa
    {
        public string i { get; set; }
        public string xi { get; set; }
        public string xd { get; set; }
        public string x { get; set; }
        public string abs { get; set; }
        public string abs_xdxi { get; set; }
        public override string ToString()
        {
            return $"{i},{xi},{xd},{x},{abs},{abs_xdxi}";
        }
    }
}
