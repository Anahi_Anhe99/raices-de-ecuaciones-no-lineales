﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManejoFunciones.Entidades
{
    public class TablaPuntoFijo
    {
        public string i { get; set; }
        public string xi { get; set; }
        public string gxi { get; set; }
        public string fxi { get; set; }
        public string abs { get; set; }
    }
}
