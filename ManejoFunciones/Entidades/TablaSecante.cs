﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManejoFunciones.Entidades
{
    public class TablaSecante
    {
        public string i { get; set; }
        public string x0 { get; set; }
        public string x1 { get; set; }
        public string x { get; set; }
        public string fx { get; set; }
        public string abs { get; set; }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", i, x0, x1, x, fx, abs);
        }
    }

}
