﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ManejoFunciones
{
    public class ConvertirCSV<T>
    {
        public string Archivo { get; set; }
        public ConvertirCSV (string archivo)
        {
            Archivo = archivo;
        }
        public bool ExportarCSV(string encabezado, List<T> datos)
        {
            try
            {
                StreamWriter file = new StreamWriter(Archivo);
                int iColCount = datos.Count;
                file.Write(encabezado);

                file.Write(file.NewLine);

                // Escribiendo todas los elementos de la lista.
                foreach (var item in datos)
                {
                    file.Write(item + file.NewLine);
                }
                file.Close();
                return true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }
        
    }
}

