﻿using org.mariuszgromada.math.mxparser;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManejoFunciones
{
    public class EvaluarFunciones
    {
        //Genera la lista de puntos que se requieren para graficar la funcion
        public List<Punto> TabularFuncion(string funcion, out string mensaje, double min = 0, double max = 0, double inc = 1f)
        {
            List<Punto> puntos = new List<Punto>();
            try
            {
                Function f = GenerarFuncion(funcion);
                for (double i = min; i < max; i += inc)
                {
                    Expression e1 = new Expression("f(" + i + ")", f);
                    puntos.Add(new Punto(i, double.Parse(e1.calculate().ToString())));
                }
                mensaje = "OK";
            }
            catch (Exception ex)
            {
                mensaje = "Error: " + ex.ToString();
            }
            return puntos;
        }

        //Se requiere de transformar una cadena de texto en una funcion entendible, para la libreria que implementamos, y así pueda evaluar funciones.
        public Function GenerarFuncion(string funcion)
        {
            
            return new Function("f(x)=" + funcion);
        }
        //Evalua Funciones funciones en un valor determinado
        public double EvaluarFuncion(string funcion, double x0)
        {
            Expression e = new Expression($"f({x0})", GenerarFuncion(funcion));
            return double.Parse(e.calculate().ToString());

        }
        //Calcula la derivada y la evalua en un número.
        public double DerivarFuncion(string funcion, double valor = 0)
        {
            Argument x = new Argument("x = " + valor);
            Expression e = new Expression($"der({funcion} , x)", x);
            return double.Parse(e.calculate().ToString());
        }
        //Calcula las constantes como pi, euler, 10^3, etc.. ocupadas para el intervalo de graficacion y los epsilons del metodo newton.
        public double EvaluarConstantes(string constante)
        {
            return  double.Parse(new Expression(constante).calculate().ToString());
        }
       
    }
}
