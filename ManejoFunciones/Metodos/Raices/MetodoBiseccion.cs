﻿using ManejoFunciones.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace ManejoFunciones.Metodos.Raices
{
    public class MetodoBiseccion
    {
        EvaluarFunciones evaluar = new EvaluarFunciones();
        double x_i;
        double x_d;
        double x;
        int i;
        double abs;
        double evaluar_xi;
        double evaluar_xd;
        double evaluar_x;
        double xs = 0;
        public string MetodoBiseccionn(string funcion, double xi, double xd, double eps, int max_it, out int total_it, out List<TablaBiseccion> datos)
        {
            i = 1;
            x_i = xi;
            x_d = xd;
            evaluar_xi = evaluar.EvaluarFuncion(funcion, xi);
            evaluar_xd = evaluar.EvaluarFuncion(funcion, xd);
            datos = new List<TablaBiseccion>();
            if ((evaluar_xi < 0 && evaluar_xd < 0) || (evaluar_xi > 0 && evaluar_xd > 0))
            {
                MessageBox.Show("La evaluación de xd y xi deben resultar con signos opuestos.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                total_it = 0;
                return "";
            }
            while (1 < max_it)
            {
                x = (x_i + x_d) / 2;
                evaluar_x = evaluar.EvaluarFuncion(funcion, x);
                abs = Math.Abs(x - xs);
                datos.Add(new TablaBiseccion
                {
                    i = i.ToString(),
                    xi = x_i.ToString().Replace(",", ""),
                    xd = x_d.ToString().Replace(",", ""),
                    x = x.ToString().Replace(",", ""),
                    abs = abs.ToString().Replace(",", ""),
                    absf = Math.Abs(evaluar_x).ToString().Replace(",", "")
                });
                if (abs < eps)
                {
                    total_it = i;
                    return x.ToString("N20");
                }
                if (evaluar_x > 0)
                {
                    x_d = x;
                    xs = x_i;
                }
                if (evaluar_x < 0)
                {
                    x_i = x;
                    xs = x_d;
                }
                i++;
            }
            total_it = i;
            return "El método no converge a una raíz";
        }

    }
}
