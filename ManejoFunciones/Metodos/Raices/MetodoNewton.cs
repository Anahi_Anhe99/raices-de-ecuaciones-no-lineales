﻿using org.mariuszgromada.math.mxparser;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManejoFunciones
{
    public class MetodoNewton
    {
        //Variables que se ocupan en el metodo de Newton Rapshon
        EvaluarFunciones evalua = new EvaluarFunciones();
        int i = 0;
        double x = 0;
        double x_0 = 0;
        double funcionEvaluada = 0;
        double derivada = 0;
        double abs = 0;
        double fxabs = 0;

        public string MetodoNewtonRapshon(string funcion, double x0, int maxit, double crit_Convergencia, double crit_Exactitud, out int totaliteraciones, out List<TablaNewton> datosNewton)
        {

            i = 1;
            x_0 = x0;
            datosNewton = new List<TablaNewton>();
            //Aplicación del algoritmo
            while (i <= maxit)
            {
                funcionEvaluada = evalua.EvaluarFuncion(funcion, x_0);

                derivada = evalua.DerivarFuncion(funcion, x_0);
                //Validar que al evaluar la derivada el resultado no sea cero y truene porque no se puede dividir entre  0 por la formula x = x0-F(x)/F'(x)
                if (derivada == 0)
                {
                    totaliteraciones = 0;
                    return "f'(" + x_0 + ") ≠ 0";
                }
                x = x_0 - (funcionEvaluada / derivada);

                abs = Math.Abs(x - x_0);
                fxabs = Math.Abs(evalua.EvaluarFuncion(funcion, x));
                //Almacena los datos de cada iteracion excepto de la primera, para exportar al csv.
                datosNewton.Add(new TablaNewton()
                {
                    i = i.ToString(),
                    xi = x_0.ToString().Replace(",", ""),
                    gxi = x.ToString().Replace(",", ""),
                    abs = abs.ToString().Replace(",", ""),
                    fxi = funcionEvaluada.ToString().Replace(",", "")
                });
                if (abs <= crit_Convergencia)
                {
                    totaliteraciones = i;
                    return x.ToString("N20");
                }
                if (fxabs <= crit_Exactitud)
                {
                    totaliteraciones = i;
                    return x.ToString("N20"); ;
                }
                i++;
                x_0 = x;
            }
            totaliteraciones = 0;
            return "El método no converge a una raíz";
        }
        public string MetodoIllinois()
        {
            return "";
        }
    }
}
