﻿using ManejoFunciones.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManejoFunciones.Metodos.Raices
{
    public class MetodoPosicionFalsa
    {
        EvaluarFunciones evalua = new EvaluarFunciones();
        double x_i;
        double x_d;
        double x;
        int i;
        double f_i;
        double f_d;
        double f;
        double diferencia;
        double abs;
        double absf;
        public string MetodoPosiciónFalsaa(string funcion, double xi, double xd, double eps, double eps1, int max_it, out int total_it, out List<TablaPosicionFalsa> datos)
        {
            i = 1;
            x_i = xi;
            x_d = xd;
            f_i = evalua.EvaluarFuncion(funcion, x_i);
            f_d = evalua.EvaluarFuncion(funcion, x_d);
            datos = new List<TablaPosicionFalsa>();
            while (i < max_it)
            {
                diferencia = f_d - f_i;
                if (diferencia == 0)
                {
                    total_it = 0;
                    return "No se puede dividir entre cero";
                }
                x = ((x_i * f_d) - (x_d * f_i)) / diferencia;
                f = evalua.EvaluarFuncion(funcion, x);
                absf = Math.Abs(f);
                abs = Math.Abs(x - x_i);
                datos.Add(new TablaPosicionFalsa
                {
                    i = i.ToString(),
                    xi=x_i.ToString().Replace(",", ""),
                    xd=x_d.ToString().Replace(",", ""),
                    x=x.ToString().Replace(",", ""),
                    abs =absf.ToString().Replace(",", ""),
                    abs_xdxi=absf.ToString().Replace(",", "")
                });
                if (absf < eps1)
                {
                    total_it = i;
                    return x.ToString("N20");
                }
                if (abs < eps)
                {
                    x = (x_d + x_i) / 2;
                    total_it = i;
                    return x.ToString("N20");
                }
                if (f_d * f > 0)
                {
                    x_d = x;
                    f_d = f;
                }
                if (f_d * f < 0)
                {
                    x_i = x;
                    f_i = f;
                }
                i++;
            }
            total_it = 0;
            return "El método no converge a una raíz";
        }
    }
}
