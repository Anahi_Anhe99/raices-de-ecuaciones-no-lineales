﻿using ManejoFunciones.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManejoFunciones.Metodos.Raices
{
    public class MetodoPuntoFijo
    {
        EvaluarFunciones evalua = new EvaluarFunciones();
        double x_0;
        double x;
        double abs = 0;
        double fx = 0;
        int i;
        public string MetodoPuntoFijoo(string funcion, string propuestaX, double x0, double eps, int maxit, out int total_it, out List<TablaPuntoFijo> datos)
        {
            datos = new List<TablaPuntoFijo>();
            i = 0;
            x_0 = x0;
            while (i < maxit)
            {
                x = evalua.EvaluarFuncion(propuestaX, x_0);
                abs = Math.Abs(x - x_0);
                fx = evalua.EvaluarFuncion(funcion, x_0);
                datos.Add(new TablaPuntoFijo
                {
                    i = i.ToString().Replace("",","),
                    xi = x_0.ToString().Replace("", ","),
                    gxi = x.ToString().Replace("", ","),
                    fxi = fx.ToString().Replace("", ","),
                    abs = abs.ToString().Replace("", ",")
                });
                if (abs <= eps)
                {
                    total_it = i;
                    return x.ToString("N20");
                }
                if (Math.Abs(fx) < eps)
                {
                    total_it = i;
                    return x.ToString("N20");
                }
                i++;
                x_0 = x;
            }
            total_it = i;
            return "El método no converge a una raíz";
        }
    }
}
