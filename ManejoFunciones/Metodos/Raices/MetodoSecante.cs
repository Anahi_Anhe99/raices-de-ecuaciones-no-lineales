﻿using ManejoFunciones.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManejoFunciones.Metodos.Raices
{
    public class MetodoSecante
    {
        EvaluarFunciones evalua = new EvaluarFunciones();
        int i = 0;
        double x = 0;
        double x_0 = 0;
        double x_1 = 0;
        double funcionx0 = 0;
        double funcionx1 = 0;
        double funcionx = 0;
        double resta;
        double abs;
        public string MetodoSecantee(string funcion, double x0, double x1, double eps, double eps1, int maxit, out int totaliteraciones, out List<TablaSecante> datosSecante)
        {
            //return raiz;
            i = 1;
            x_0 = x0;
            x_1 = x1;
            datosSecante = new List<TablaSecante>();
            //Aplicacion del Algoritmo
            while (i <= maxit)
            {
                funcionx0 = evalua.EvaluarFuncion(funcion, x_0);
                funcionx1 = evalua.EvaluarFuncion(funcion, x_1);
                resta = funcionx1 - funcionx0;
                if (resta == 0)
                {
                    totaliteraciones = 0;
                    return "No se puede dividir entre cero";
                }
                x = x_1 - ((x_1 - x_0) * funcionx1) / resta;
                funcionx = evalua.EvaluarFuncion(funcion, x);

                abs = Math.Abs(x - x_1);
                datosSecante.Add(new TablaSecante()
                {
                    i = i.ToString().Replace(",", ""),
                    x0 = x_0.ToString().Replace(",", ""),
                    x1 = x_1.ToString().Replace(",", ""),
                    x = x.ToString().Replace(",", ""),
                    abs = abs.ToString().Replace(",", ""),
                    fx = funcionx.ToString().Replace(",", ""),
                });

                if (abs <= eps || Math.Abs(funcionx) <= eps1 || Math.Abs(funcionx) == 0)
                {
                    totaliteraciones = i;
                    return x.ToString("N20");
                }
                x_0 = x_1;
                x_1 = x;
                i++;
            }

            totaliteraciones = 0;
            return "El método no converge a una raíz";
        }
    }
}
