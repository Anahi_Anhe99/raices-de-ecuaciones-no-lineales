﻿using ManejoFunciones.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManejoFunciones.Metodos.Raices
{
    public class MetodoSteffensen
    {
        EvaluarFunciones evalua = new EvaluarFunciones();
        int i;
        double x = 0d;
        double x_0 = 0d;
        double x_1;
        double x_2;
        double abs = 0d;
        double fx = 0d;
        public string MetodoSteffenseen(string funcion, string propuestaX, double x0, double eps, int maxit, out int totaliteraciones, out List<TablaSteffensen> datosSecante)
        {
            i = 1;
            x_0 = x0;
            datosSecante = new List<TablaSteffensen>();
            while (i < maxit)
            {
                x_1 = evalua.EvaluarFuncion(propuestaX, x_0);
                x_2 = evalua.EvaluarFuncion(propuestaX, x_1);
                x = x_0 - ((x_1 - x_0)*(x_1 - x_0)/( x_2 - (2d * x_1) + x_0));
                abs = Math.Abs( x - x_0);
                fx = evalua.EvaluarFuncion(funcion, x);
                datosSecante.Add(new TablaSteffensen
                {
                    i = i.ToString(),
                    x0=x_0.ToString().Replace(",",""),
                    x1=x_1.ToString().Replace(",", ""),
                    x2=x_2.ToString().Replace(",", ""),
                    x=x.ToString().Replace(",", ""),
                    abs=abs.ToString().Replace(",", ""),
                    fx=fx.ToString().Replace(",", ""),
                });
                if (abs<eps||Math.Abs(fx)<eps)
                {
                    totaliteraciones = i;
                    return x.ToString("N20");
                }
                i++;
                x_0 = x;
            }

            totaliteraciones = 0;
            return "El método no converge a ninguna raíz";
        }
    }
}
